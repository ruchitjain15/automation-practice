package stepDefinition;

import AutomationPracticePages.AuthenticationPage;
import AutomationPracticePages.CreateAccountPage;
import AutomationPracticePages.Homepage;
import AutomationPracticePages.UserHomepage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Steps {

	@Given("^User is on Create Account page$")
	public void navigateToCreateAccount() {
		new Homepage().navigateToSignIn();
		new AuthenticationPage().createAccount();

	}

	@When("^User enters all details and clicks on Register button$")
	public void registerAccountDetails() {
		new CreateAccountPage().createAccount();
	}

	@Then("^User Registration process completes successfully$")
	public void authenticateSuccessMessage() {
		new UserHomepage().validateSignInUserName();
	}
}
