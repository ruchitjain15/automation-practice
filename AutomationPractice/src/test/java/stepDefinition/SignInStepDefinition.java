package stepDefinition;

import AutomationPracticePages.AuthenticationPage;
import AutomationPracticePages.Homepage;
import AutomationPracticePages.UserHomepage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SignInStepDefinition {

	@Given("^User is on Authentication page$")
	public void LoginPage() {
		new Homepage().navigateToSignIn();
	}

	@When("^User enters email and \"(.*)\" and click on Sign In button$")
	public void enterDetailsLoginPage(String password) {
		new AuthenticationPage().signIn(password);
	}

	@Then("^User log in successfully$")
	public void loginSuccess() {
		new UserHomepage().validateSignInUserName();
	}
}
