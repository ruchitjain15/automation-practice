package stepDefinition;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import Utilities.GenerateDynamicData;
import Utilities.GetConfigData;
import Utilities.JSONReader;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.github.bonigarcia.wdm.WebDriverManager;

public class BeforeAfter {

	static public WebDriver driver;

	@Before
	public void setup() {

		try {
			WebDriverManager.chromedriver().setup();
			WebDriverManager.firefoxdriver().setup();
			String browser = GetConfigData.getConfigData().get("BROWSER");
			String url = GetConfigData.getConfigData().get("URL");
			if (browser.equalsIgnoreCase("Chrome")) {
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("Firefox")) {
				driver = new FirefoxDriver();
			}

			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.manage().deleteAllCookies();

			driver.get(url);

		} catch (Exception e) {
			Assert.fail("Error Page loading... ");
			e.printStackTrace();
			System.exit(0);
		}
	}

	@After
	public void tearDown() {
		try {
			driver.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
