package Utilities;

import java.io.File;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;

public class GetConfigData {
	static HashMap<String, String> configData = new HashMap<String, String>();

	static public HashMap<String, String> getConfigData() {

		try {
			File f = new File("config.xml");

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(f);
			doc.getDocumentElement().normalize();

			// String numberOfThreads =
			// doc.getElementsByTagName("NUMBEROFTHREADS").item(0).getTextContent();
			// configData.put("NUMBEROFTHREADS", numberOfThreads);

			// for (int i = 1; i <= Integer.parseInt(numberOfThreads); i++) {
			configData.put("BROWSER", doc.getElementsByTagName("BROWSER").item(0).getTextContent());
			configData.put("URL", doc.getElementsByTagName("URL").item(0).getTextContent());
			// }
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error while getting data for Tag..." + e.getLocalizedMessage());
		}
		return configData;
	}
}
