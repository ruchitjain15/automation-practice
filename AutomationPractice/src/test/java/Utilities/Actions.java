package Utilities;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import stepDefinition.BeforeAfter;

public class Actions {

	static WebDriverWait wait;

	static public void click(WebElement element) {
		try {
			element.click();
		} catch (Exception e) {
			Assert.fail(e.getMessage());
			e.printStackTrace();
		}
	}

	static public void sendKeys(WebElement element, String key) {
		try {
			element.sendKeys(key);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	static public boolean visibilityOfElement(WebElement element) {
		try {
			wait = new WebDriverWait(BeforeAfter.driver, 20);
			wait.until(ExpectedConditions.visibilityOf(element));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	static public boolean isClickable(WebElement element) {
		try {
			wait = new WebDriverWait(BeforeAfter.driver, 20);
			wait.until(ExpectedConditions.elementToBeClickable(element));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	static public void clear(WebElement element) {
		try {
			element.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	static public boolean isSelected(WebElement element) {
		try {
			return element.isSelected();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
