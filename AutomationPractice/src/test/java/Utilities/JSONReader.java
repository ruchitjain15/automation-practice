package Utilities;

import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class JSONReader {

	public static String filepath = new File("").getAbsolutePath() + "/TestData/AccountInfo.json";
	public static Map<String, String> accountInfo = new HashMap<String, String>();
	// static List<Map<String, String>> allAccountInfo = new ArrayList<Map<String,
	// String>>();

	@SuppressWarnings("unchecked")
	static public Map<String, String> getAccountInfoData() {

		System.out.println("Executing json reader method........");
		System.out.println("Account Info.. " + accountInfo);

		try {
			JSONParser jparser = new JSONParser();
			FileReader fread = new FileReader(filepath);
			Object obj = jparser.parse(fread);

			// JSONArray jarr = (JSONArray)obj;

			System.out.println("Object " + obj);
			JSONArray jarr = new JSONArray();
			jarr.add(obj);
			int len = jarr.size();

			for (int i = 0; i < len; i++) {
				JSONObject job = (JSONObject) jarr.get(i);
				JSONArray jarr1 = (JSONArray) job.get("createAccount");
				for (int j = 0; j < jarr1.size(); j++) {
					JSONObject jobj = (JSONObject) jarr1.get(j);
					// System.out.println("Data: " + jobj);

					accountInfo.put("Title", (String) jobj.get("Title"));
					accountInfo.put("FirstName", (String) jobj.get("FirstName"));
					accountInfo.put("LastName", (String) jobj.get("LastName"));
					accountInfo.put("Password", (String) jobj.get("Password"));
					accountInfo.put("Email", GenerateDynamicData.createDynamicEmail());

					// setting up email Id dynamically with timestamp

//					System.out.println("In JSON Reader..... + " + jobj.get("Email"));
//					jobj.put("Email", accountInfo.get("Email"));
//					
//					System.out.println("In JSON Reader 2..... + " + jobj.get("Email"));
//					
					accountInfo.put("DOBDay", (String) jobj.get("DOBDay"));
					accountInfo.put("DOBMon", (String) jobj.get("DOBMon"));
					accountInfo.put("DOBYear", (String) jobj.get("DOBYear"));
					accountInfo.put("Company", (String) jobj.get("Company"));
					accountInfo.put("Address", (String) jobj.get("Address"));
					accountInfo.put("City", (String) jobj.get("City"));
					accountInfo.put("State", (String) jobj.get("State"));
					accountInfo.put("PostalCode", (String) jobj.get("PostalCode"));
					accountInfo.put("Country", (String) jobj.get("Country"));
					accountInfo.put("AdditionalInformation", (String) jobj.get("AdditionalInformation"));
					accountInfo.put("HomePhone", (String) jobj.get("HomePhone"));
					accountInfo.put("MobilePhone", (String) jobj.get("MobilePhone"));
					accountInfo.put("AddressAlias", (String) jobj.get("AddressAlias"));

					// allAccountInfo.add(accountInfo);

				}
			}

			// return allAccountInfo;

			System.out.println("Account Info set... + " + accountInfo);
			return accountInfo;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
