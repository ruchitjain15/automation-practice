package Utilities;

import java.sql.Timestamp;

public class GenerateDynamicData {

	static public String createDynamicEmail() {
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		String timeStamp = ts.toString();
		timeStamp = timeStamp.replace(" ", "");
		timeStamp = timeStamp.replace(":", "");
		timeStamp = timeStamp.replace(".", "");
		timeStamp = timeStamp.replace("-", "");


		System.out.println("Email Id created as : " + "test" + timeStamp + "@soprasteria.com");
		return "test" + timeStamp + "@soprasteria.com";

	}

	public static void main(String... str) {
		createDynamicEmail();
	}
}
