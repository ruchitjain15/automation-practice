package Utilities;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import stepDefinition.BeforeAfter;

public class TakeSnapShot {

	static public void takeSnap(String path) {
		try {
			TakesScreenshot tshot = (TakesScreenshot) (BeforeAfter.driver);
			File srcFile = tshot.getScreenshotAs(OutputType.FILE);

			File dstFile = new File(path);
			FileUtils.copyFile(srcFile, dstFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
