package AutomationPracticePages;

import org.junit.Assert;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utilities.Actions;
import Utilities.TakeSnapShot;
import stepDefinition.BeforeAfter;

public class Homepage {
	
	public Homepage() {
		PageFactory.initElements(BeforeAfter.driver, this);
	}
	
	WebDriverWait wait; 
	
	@FindBy(linkText = "Sign in")
	WebElement signInButton;
	
	
	public void navigateToSignIn() {
		if(!Actions.visibilityOfElement(signInButton)) {
			TakeSnapShot.takeSnap("E://homepageTestFail.png");
			Assert.fail("Sign IN button not visible");
		}
		Actions.click(signInButton);	
	}
	
}
