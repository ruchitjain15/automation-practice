package AutomationPracticePages;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Utilities.Actions;
import Utilities.JSONReader;
import Utilities.TakeSnapShot;
import stepDefinition.BeforeAfter;

public class AuthenticationPage {

	public AuthenticationPage() {
		PageFactory.initElements(BeforeAfter.driver, this);
	}

	@FindBy(id = "email_create")
	WebElement createAcntEmail;

	@FindBy(id = "SubmitCreate")
	WebElement createAcntButn;

	@FindBy(id = "email")
	WebElement email;

	@FindBy(id = "passwd")
	WebElement password;

	@FindBy(id = "SubmitLogin")
	WebElement signIn;

	@FindBy(xpath = "//div[@id='center_column']//h1")
	WebElement authenticationText;

	 public void createAccount() {

		 
		 
		 
		if(!Actions.visibilityOfElement(authenticationText)) {
			TakeSnapShot.takeSnap("E://AuthenticationtextInvisible.png");
			Assert.fail("Authentication text not visible..");
		}
		Actions.clear(createAcntEmail);
		
		//setting up email Id dynamically with timestamp
		JSONReader.getAccountInfoData();
		

		Actions.sendKeys(createAcntEmail, JSONReader.accountInfo.get("Email"));
		
		if (Actions.isClickable(createAcntButn)) {
			Actions.click(createAcntButn);
		} else {
			TakeSnapShot.takeSnap("E://CreateAcntButtonNonClickable.png");
			Assert.fail("Create Account Button not clickable..");
		}

	}

	public void signIn(String passwd) {
		Actions.sendKeys(email, JSONReader.accountInfo.get("Email"));
		Actions.sendKeys(password, passwd);
		Actions.click(signIn);
	}

}
