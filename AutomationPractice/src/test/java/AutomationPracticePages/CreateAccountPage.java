package AutomationPracticePages;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utilities.Actions;
import Utilities.JSONReader;
import Utilities.TakeSnapShot;
import stepDefinition.BeforeAfter;

public class CreateAccountPage {

	public CreateAccountPage() {
		PageFactory.initElements(BeforeAfter.driver, this);
	}

	@FindBy(id = "id_gender1")
	WebElement title1;

	@FindBy(id = "id_gender2")
	WebElement title2;

	@FindBy(id = "customer_firstname")
	WebElement firstName;

	@FindBy(id = "customer_lastname")
	WebElement lastName;

	@FindBy(id = "email")
	WebElement email;

	@FindBy(id = "passwd")
	WebElement password;

	@FindBy(id = "days")
	WebElement days;

	@FindBy(id = "months")
	WebElement months;

	@FindBy(id = "years")
	WebElement years;

	@FindBy(id = "newsletter")
	WebElement newsLetterCheckbox;

	@FindBy(id = "firstname")
	WebElement yourAddFirstName;

	@FindBy(id = "lastname")
	WebElement yourAddLastName;

	@FindBy(id = "company")
	WebElement company;

	@FindBy(id = "address1")
	WebElement address;

	@FindBy(id = "city")
	WebElement city;

	@FindBy(id = "id_state")
	WebElement state;

	@FindBy(id = "postcode")
	WebElement postcode;

	@FindBy(id = "id_country")
	WebElement country;

	@FindBy(id = "other")
	WebElement additionalInfo;

	@FindBy(id = "phone")
	WebElement homePhone;

	@FindBy(id = "phone_mobile")
	WebElement mobilePhone;

	@FindBy(id = "alias")
	WebElement addressAlias;

	@FindBy(xpath = "//button[@id='submitAccount']//span")
	WebElement register;

	@FindBy(xpath = "//div[@id='noSlide']//h1")
	WebElement createAccountText;

	WebDriverWait wait;

	public void createAccount() {
		if (!Actions.visibilityOfElement(createAccountText)) {
			TakeSnapShot.takeSnap("E://createAcntTextInvisible.png");
			Assert.fail("Create Account Text not visible ..");
		}

		Map<String, String> data = new HashMap<String, String>(JSONReader.accountInfo);
	
		if (data.get("Title").equalsIgnoreCase("Mr.")) {
			Actions.click(title1);
		} else {
			Actions.click(title2);
		}

		Actions.sendKeys(firstName, data.get("FirstName"));
		Actions.sendKeys(lastName, data.get("LastName"));

		System.out.println("email :");
		String emailText = email.getAttribute("value");

		System.out.println("email :" + emailText);

		if (!emailText.equalsIgnoreCase(data.get("Email"))) {
			System.exit(0);
		}

		Actions.sendKeys(password, data.get("Password"));
		Actions.sendKeys(days, data.get("DOBDay"));
		Actions.sendKeys(months, data.get("DOBMon"));
		Actions.sendKeys(years, data.get("DOBYear"));

		if (!Actions.isSelected(newsLetterCheckbox)) {
			Actions.click(newsLetterCheckbox);
		}

		String fname = yourAddFirstName.getAttribute("value");
		String lname = yourAddLastName.getAttribute("value");

		if (!fname.equals(data.get("FirstName"))) {
			System.out.println("Firstname mismatch");
		}

		if (!lname.equals(data.get("LastName"))) {
			System.out.println("Lastname mismatch");
		}
		

		Actions.sendKeys(company, data.get("Company"));
		Actions.sendKeys(address, data.get("Address"));
		Actions.sendKeys(city, data.get("City"));
		Actions.sendKeys(state, data.get("State"));
		Actions.sendKeys(postcode, data.get("PostalCode"));
		Actions.sendKeys(country, data.get("Country"));
		Actions.sendKeys(additionalInfo, data.get("AdditionalInformation"));
		Actions.sendKeys(homePhone, data.get("HomePhone"));
		Actions.sendKeys(mobilePhone, data.get("MobilePhone"));
		Actions.sendKeys(addressAlias, data.get("AddressAlias"));

		if(Actions.isClickable(register)) {
			Actions.click(register);
		}else{
			TakeSnapShot.takeSnap("E://registerButtonNonClickable.png");
			Assert.fail("Register button not clickable...");
		}

	}

}
