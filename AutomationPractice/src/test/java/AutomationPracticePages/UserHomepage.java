package AutomationPracticePages;


import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Utilities.Actions;
import Utilities.JSONReader;
import Utilities.TakeSnapShot;
import stepDefinition.BeforeAfter;

public class UserHomepage {

	public UserHomepage(){
		PageFactory.initElements(BeforeAfter.driver, this);
	}
	
	@FindBy(xpath = "//a[@class='account']//span")
	WebElement userName;
	
	public void validateSignInUserName() {
		if(!Actions.visibilityOfElement(userName)){
			TakeSnapShot.takeSnap("E://UsernameNotVisible.png");
			Assert.fail("User name not visible.. Authentication failed");
		}
		
		String username = userName.getText();
		if(username.equalsIgnoreCase(JSONReader.accountInfo.get("FirstName") + " " + JSONReader.accountInfo.get("LastName") )) {
			
		}else {
			TakeSnapShot.takeSnap("E://usernameMismatch.png");
			Assert.fail("Sign in username mismatch...");
		}

	}
}
