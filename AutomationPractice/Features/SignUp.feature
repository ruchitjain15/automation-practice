Feature: Registration 


Scenario: Verify Register button functionality on Create Account Page 
	Given User is on Create Account page 
	When User enters all details and clicks on Register button 
	Then User Registration process completes successfully 
	
	
Scenario Outline: Verify Login Functionality 
	Given User is on Authentication page 
	When User enters email and "<password>" and click on Sign In button 
	Then User log in successfully 
	
	Examples: 
		| password | 
		| Sopra@12 |
 